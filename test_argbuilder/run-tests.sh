#!/bin/bash

set -e

test -d .venv || virtualenv .venv
. .venv/bin/activate
pip install -q --upgrade pip

root=$(pwd)
TEST_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export PYTHONPATH=${root}:${PYTHONPATH}

if [[ -f ${root}/argbuilder.py ]]; then
  pip install -q -r ${root}/requirements.txt -r ${root}/test_argbuilder/test-requirements.txt
elif [[ -f confluence-core/confluence-webapp/src/main/resources/argbuilder.py ]]; then
  ARGBUILDER_DIR="confluence-core/confluence-webapp/src/main/resources"
  export PYTHONPATH=${ARGBUILDER_DIR}:${PYTHONPATH}
  pip install -q -r ${ARGBUILDER_DIR}/requirements.txt -r ${TEST_DIR}/test-requirements.txt
else
  echo "Unable to find argbuilder"
  exit 1
fi

nose2 -v --plugin nose2.plugins.junitxml --junit-xml -c ${TEST_DIR}/unittest.cfg -s ${TEST_DIR}
